(function () {

    "use strict";

    var azure = require("azure-storage");

    var serialize = function (propObj) {
        var fullProps = {};
        Object.keys(propObj).forEach(function (key) {
            var propVal = propObj[key];
            if (key === "PartitionKey" || key === "RowKey") {
                propVal = encodeURIComponent(propVal);
            }

            var type = typeof propVal;
            if (key === ".metadata") {
                fullProps[key] = {
                    etag: propVal.etag
                };
            }
            else if (type === "string" || propVal instanceof String) {
                fullProps[key] = azure.TableUtilities.entityGenerator.String(propVal);
            }
            else if (type === "object" && propVal instanceof Date) {
                fullProps[key] = azure.TableUtilities.entityGenerator.DateTime(propVal);
            }
            else if (type === "number") {
                fullProps[key] = azure.TableUtilities.entityGenerator.Double(propVal);
            }
            else if (type === "boolean") {
                fullProps[key] = azure.TableUtilities.entityGenerator.Boolean(propVal);
            }
            else {
                throw new Error("Cannot convert value with key: " + key + " and type: " + type);
            }
        });
        return fullProps;
    };

    var deserialize = function (fullProps) {
        //Borrowed from npm:azure-storage/lib/services/table/internal/edmhandler.js
        var deserializeOne = function (type, value) {
            var EdmType = azure.TableUtilities.EdmType;
            switch (type) {
                case EdmType.BINARY:
                    return new Buffer(value, "base64");
                case EdmType.DATETIME:
                    return new Date(value);
                case EdmType.GUID:
                    return value;
                case EdmType.DOUBLE:
                    // Account for Infinity and NaN:
                    if (typeof value !== "number") {
                        return parseFloat(value);
                    }
                    return value;
                default:
                case EdmType.INT32:
                case EdmType.INT64:
                case EdmType.STRING:
                case EdmType.BOOLEAN:
                    return value;
            }
        };
        var propsObj = {};
        Object.keys(fullProps).forEach(function (key) {
            var fullPropVal = fullProps[key];
            if (key === "Timestamp") {
                return;
            }

            var propVal;
            if (key === ".metadata") {
                propVal = {
                    etag: fullPropVal.etag
                };
            }
            else {
                propVal = deserializeOne(fullPropVal.$, fullPropVal._);
                if (key === "PartitionKey" || key === "RowKey") {
                    propVal = decodeURIComponent(propVal);
                }
            }

            propsObj[key] = propVal;
        });

        return propsObj;

    };

    var queryType = function (type) {
        switch(type) {
            case "boolean":
                return "?bool?";
            default:
                return "?";
        }
    };

    exports.serialize = serialize;
    exports.deserialize = deserialize;
    exports.encodeKey = encodeURIComponent;
    exports.decodeKey = decodeURIComponent;
    exports.queryType = queryType;
})();