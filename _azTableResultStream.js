(function () {

    "use strict";

    var azTableModel = require("./_azTableDataSerializer");
    var oneByOne = require("@withjoy/oneByOne");

    exports.buildResultStream = function (azService, tableName, query) {
        var service = azService.nativeObject.bind(azService);
        var continuation = null;
        var arrayStream = oneByOne.createObjectSourceStream({highWaterMark: 0}, function (pushDataCallback) {
            return service().queryEntities(tableName, query, continuation, function (err, result) {
                if (err) {
                    return pushDataCallback(err);
                }
                continuation = result.continuationToken;
                pushDataCallback(null, result.entries, !continuation);
            });
        });

        var arraySplitTransform = oneByOne.createArraySplitTransformStream({highWaterMark: 0});
        var mapTransform = oneByOne.createObjectMapTransformStream({highWaterMark: 0}, function (entry, callback) {
            var propsObj;

            try {
                propsObj = azTableModel.deserialize(entry);
            }
            catch (err) {
                return callback(err);
            }

            return callback(null, propsObj);
        });

        return arrayStream.pipe(arraySplitTransform).pipe(mapTransform);
    };
})();