(function () {
    "use strict";

    var AzContainer = function (azService, containerName) {
        var This = this;

        This._azService = azService;
        This._containerName = containerName;
    };

    AzContainer.prototype.initialize = function (callback) {
        var This = this;
        This._azService.createContainerIfNotExists(This._containerName, callback);
    };

    AzContainer.prototype.finalize = function (callback) {
        var This = this;
        This._azService.destroyContainer(This._containerName, callback);
    };

    AzContainer.prototype.generateSharedAccessSignature = function (blobId, policy) {
        var This = this;
        return This._azService.generateSharedAccessSignature(This._containerName, blobId, policy);
    };

    AzContainer.prototype.createBlobWritableStream = function (targetBlobId, options) {
        var This = this;
        return This._azService.createBlobWritableStream(This._containerName, targetBlobId, options);
    };

    AzContainer.prototype.createBlobWithReadableStream = function (blobId, stream, options, callback) {
        var This = this;
        return This._azService.createBlobWithReadableStream(
            This._containerName,
            blobId,
            stream,
            options,
            callback
        );
    };

    AzContainer.prototype.findBlobs = function () {
        var This = this;
        return This._azService.findBlobs(This._containerName);
    };

    AzContainer.prototype.getUrl = function (blobId) {
        var This = this;
        return This._azService.getUrl(This._containerName, blobId);
    };

    AzContainer.prototype.getBlobProperties = function (blobId, callback) {
        var This = this;
        return This._azService.getBlobProperties(This._containerName, blobId, callback);
    };

    AzContainer.prototype.getContainerName = function () {
        var This = this;
        return This._containerName;
    };

    AzContainer.prototype.getService = function () {
        var This = this;
        return This._azService;
    };

    AzContainer.prototype.deleteBlob = function (blobId, callback) {
        var This = this;
        return This._azService.deleteBlob(This._containerName, blobId, callback);
    };


    exports.Container = AzContainer;
})();
