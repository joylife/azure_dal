(function () {
    "use strict";


    var extend = require("extend");
    const promisify = require("@withjoy/promisify");
    /*
     * Creates a new AzTable instance
     * @param service AzTableService Object
     * @param tableName
     * @constructor
     */
    var AzTable = function (azService, tableName) {
        var This = this;
        This._azService = azService;
        This._tableName = tableName;
    };

    AzTable.prototype.insertRow = promisify(function (partitionKey, rowKey, props, callback) {
        var This = this;
        return This._azService.insertEntity(This._tableName, extend(true, {}, props, {
            PartitionKey: partitionKey,
            RowKey: rowKey
        }), callback);
    });

    AzTable.prototype.deleteRow = promisify(function (partitionKey, rowKey, callback) {
        var This = this;
        return This._azService.deleteEntity(This._tableName, {
            PartitionKey: partitionKey,
            RowKey: rowKey
        }, callback);
    });

    AzTable.prototype.mergeRow = function (partitionKey, rowKey, props, callback) {
        var This = this;
        return This._azService.mergeEntity(This._tableName, extend(true, {}, props, {
            PartitionKey: partitionKey,
            RowKey: rowKey
        }), callback);
    };

    AzTable.prototype.insertOrReplaceRow = function (partitionKey, rowKey, props, callback) {
        var This = this;
        return This._azService.insertOrReplaceEntity(This._tableName, extend(true, {}, props, {
            PartitionKey: partitionKey,
            RowKey: rowKey
        }), callback);
    };

    AzTable.prototype.findRowsByPartitionKey = function (partitionKey) {
        var This = this;
        return This._azService.findRowsByPartitionKey(This._tableName, partitionKey);
    };

    AzTable.prototype.findRowsByRowKey = function (rowKey) {
        var This = this;
        return This._azService.findRowsByRowKey(This._tableName, rowKey);
    };

    AzTable.prototype.findRow = function (partitionKey, rowKey, callback) {
        var This = this;
        return This._azService.retrieveEntity(This._tableName, partitionKey, rowKey, callback);
    };

    AzTable.prototype.findAllRows = function () {
        var This = this;
        return This._azService.findAllRows(This._tableName);
    };

    AzTable.prototype.findRowsWithColumnValue = function (columnName, columnValue) {
        var This = this;
        return This._azService.findRowsWithColumnValue(This._tableName, columnName, columnValue);
    };

    AzTable.prototype.findRowsByRowKeyAndColumnValue = function (rowKey, columnName, columnValue) {
        var This = this;
        return This._azService.findRowsByRowKeyAndColumnValue(
            This._tableName,
            rowKey,
            columnName,
            columnValue
        );
    };

    AzTable.prototype.findRowsByPartitionKeyAndColumnValue = function (partitionKey, columnName, columnValue) {
        var This = this;
        return This._azService.findRowsByPartitionKeyAndColumnValue(
            This._tableName,
            partitionKey,
            columnName,
            columnValue
        );
    };

    AzTable.prototype.findRowsByRowKeyOrColumnValue = function (rowKey, columnName, columnValue) {
        var This = this;
        return This._azService.findRowsByRowKeyOrColumnValue(
            This._tableName,
            rowKey,
            columnName,
            columnValue
        );
    };

    AzTable.prototype.findRowsByPartitionKeyOrColumnValue = function (partitionKey, columnName, columnValue) {
        var This = this;
        return This._azService.findRowsByPartitionKeyOrColumnValue(
            This._tableName,
            partitionKey,
            columnName,
            columnValue
        );
    };

    AzTable.prototype.findRowsByPartitionKeyLessThanEqualTo = function (partitionKey) {
        let This = this;
        return This._azService.findRowsByPartitionKeyLessThanEqualTo(
            This._tableName,
            partitionKey
        );
    };
    
    exports.Table = AzTable;
})();
