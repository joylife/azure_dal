"use strict";

let AzQueue = function(azService, queueName) {
    this._azService = azService;
    this._queueName = queueName;
};

AzQueue.prototype.peakMessage = function() {
    return this._azService.peakMessages(this._queueName);
};

//does not include a visibility timeout, will show before any visibilitytimeout messages
AzQueue.prototype.createMessage = function(message) {
    return this._azService.createMessage(this._queueName, message);
};

AzQueue.prototype.createMessageWithDelay = function(message, timeToShowMessage) {
    return this._azService.createMessageWithDelay(this._queueName, message, timeToShowMessage);
};

AzQueue.prototype.getMessages = function() {
    return this._azService.getMessages(this._queueName);
};

//this does not exist in my system right now but may in the future
AzQueue.prototype.updateMessage = function(message, timeToShowMessage) {
    let messageId = message.messageid;
    let popReceipt = message.popreceipt;
    let visibilityTimeout = timeToShowMessage - Date.now();
    return this._azService.updateMessage(this._queueName, messageId, popReceipt, visibilityTimeout);
};

AzQueue.prototype.deleteMessage = function (message) {
    return this._azService.deleteMessage(this._queueName, message.messageId, message.popReceipt);
};

exports.Queue = AzQueue;
