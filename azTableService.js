(function () {

    "use strict";
    const azure = require("azure-storage");
    const oneByOne = require("@withjoy/oneByOne");
    const azTable = require("./azTable");
    const azTableResultStream = require("./_azTableResultStream");
    const azTableDataSerializer = require("./_azTableDataSerializer");
    const promisify = require("@withjoy/promisify");
    /**
     *
     * @param options Should contain: azureStorageAccount, azureStorageAccessKey
     * @constructor
     */

     var retryOptions = {
         retries: 3,
         minTimeout: 2000,
         maxTimeout: 10000,
         randomize: true
     };

    const AzTableService = function (consoleObj, options) {
        const This = this;
        This._console = consoleObj || console;
        let retryOperations = new azure.ExponentialRetryPolicyFilter(retryOptions);
        This._service = options.nativeService || new azure.createTableService(
            options.azureStorageAccount,
            options.azureStorageAccessKey
        ).withFilter(retryOperations);
        This._shouldMaintainInverseMap = !!options.shouldMaintainInverseMap;
        This._shouldRedirectReadsToInverseMap =
            This._shouldMaintainInverseMap && !!options.shouldRedirectReadsToInverseMap;
    };


    /**
     * Creates a aztable if it doesnot exist and returns a AzTable instance
     * @param tableName
     * @param callback Form (err, wasCreated, azTableObj)
     */
    AzTableService.prototype.createTableIfNotExists = function (tableName, callback) {
        const This = this;
        return This._service.createTableIfNotExists(tableName, function (err, result) {
            if (err) {
                return setImmediate(callback.bind(null, err));
            }

            if (!This._shouldMaintainInverseMap) {
                return callback(null, result);
            }
            This._service.createTableIfNotExists(tableName + "Inverse", function (err) {
                if (err) {
                    return callback(err);
                }
                var table = new azTable.Table(This, tableName);
                return setImmediate(callback.bind(null, null, result, table));
            });
        });
    };

    AzTableService.prototype.nativeObject = function () {
        const This = this;
        return This._service;
    };

    AzTableService.prototype._invertProps = function (props) {
        return {
            PartitionKey: props.RowKey,
            RowKey: props.PartitionKey
        };
    };

    AzTableService.prototype._invertTableName = function (tableName) {
        return `${tableName}Inverse`;
    };

    AzTableService.prototype._insertOrReplaceInverseEntity = function (tableName, props, callback) {
        const This = this;
        if (!This._shouldMaintainInverseMap) {
            return callback(null);
        }


        const inverseProps = This._invertProps(props);
        const inverseEntity = azTableDataSerializer.serialize(inverseProps);

        This._console.info("Adding inverted entry: " + JSON.stringify(inverseProps));
        return This._service.insertOrReplaceEntity(This._invertTableName(tableName), inverseEntity, callback);
    };

    AzTableService.prototype._deleteInverseEntity = function (tableName, props, callback) {
        const This = this;
        if (!This._shouldMaintainInverseMap) {
            return callback(null);
        }

        const inverseProps = This._invertProps(props);
        const inverseEntity = azTableDataSerializer.serialize(inverseProps);

        This._console.info("Deleting inverted entry: " + JSON.stringify(inverseProps));
        return This._service.deleteEntity(This._invertTableName(tableName), inverseEntity, function (err) {
            /**
             * This is temporary to allow for inconsistencies while the migration happens
             * otherwise we want a hard fail
             */
            This._console.error("Ignoring error deleting inverted entity");
            This._console.error(err);
            return callback();
        });
    };

    AzTableService.prototype.insertEntity = promisify(function (tableName, props, callback) {
        const This = this;

        This._insertOrReplaceInverseEntity(tableName, props, function (err) {
            if (err) {
                return callback.apply(this, arguments);
            }

            This._service.insertEntity(tableName, azTableDataSerializer.serialize(props), callback);
        });
    });

    AzTableService.prototype.insertOrReplaceEntity = function (tableName, props, callback) {
        const This = this;
        return This._insertOrReplaceInverseEntity(tableName, props, function (err) {
            if (err) {
                return callback.apply(this, arguments);
            }

            return This._service.insertOrReplaceEntity(tableName, azTableDataSerializer.serialize(props), callback);
        });
    };

    AzTableService.prototype.deleteEntity = function (tableName, props, callback) {
        const This = this;
        return This._deleteInverseEntity(tableName, props, function (err) {
            if (err) {
                return callback.apply(this, arguments);
            }

            return This._service.deleteEntity(tableName, azTableDataSerializer.serialize(props), callback);
        });
    };

    AzTableService.prototype.mergeEntity = function (tableName, props, callback) {
        const This = this;
        return This._insertOrReplaceInverseEntity(tableName, props, function (err) {
            if (err) {
                return callback.apply(this, arguments);
            }

            return This._service.mergeEntity(
                tableName,
                azTableDataSerializer.serialize(props),
                function (err) {
                    if (err) {
                        return setImmediate(callback.bind(null, err));
                    }
                    return setImmediate(callback.bind(null, null));
                }
            );
        });

    };

    AzTableService.prototype.retrieveEntity = function (tableName, partitionKey, rowKey, callback) {
        const This = this;
        This._service.retrieveEntity(
            tableName,
            azTableDataSerializer.encodeKey(partitionKey),
            azTableDataSerializer.encodeKey(rowKey),
            function (err, result) {
                if (err) {
                    return setImmediate(callback.bind(null, err));
                }
                return setImmediate(callback.bind(null, null, azTableDataSerializer.deserialize(result)));
            }
        );
    };

    AzTableService.prototype._findRows = function (tableName, q) {
        const This = this;
        return azTableResultStream.buildResultStream(This, tableName, q);
    };

    AzTableService.prototype.findAllRows = function (tableName) {
        const This = this;
        var q = new azure.TableQuery();
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsByPartitionKey = function (tableName, partitionKey) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where("PartitionKey eq ?", azTableDataSerializer.encodeKey(partitionKey));
        return This._findRows(tableName, q);
    };

    AzTableService.prototype._findRowsByRowKeyInverted = function (tableName, rowKey) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where("PartitionKey eq ?", azTableDataSerializer.encodeKey(rowKey));

        /**
         * Once we get rows from the inverted lookup we need to perform a forward lookup on the individual entities.
         */
        return This._findRows(This._invertTableName(tableName), q).pipe(
            oneByOne.createObjectMapTransformStream({highWaterMark: 0},
                function (invertedEntity, callback) {

                    This._console.info("Reverting inverted entry: " + JSON.stringify({
                        PartitionKey: invertedEntity.PartitionKey,
                        RowKey: invertedEntity.RowKey
                    }));

                    return This.retrieveEntity(tableName, invertedEntity.RowKey, invertedEntity.PartitionKey, function (err) {
                        if (err) {
                            This._console.error("Error reinverting table entry");
                            This._console.error(err);
                            return callback(err);
                        }
                        return callback.apply(this, arguments);
                    });
                }
            )
        );
    };

    AzTableService.prototype.findRowsByRowKey = function (tableName, rowKey) {
        const This = this;

        /**
         * If we are maintaining a inverted table, then lookup in that table, instead of the forward table.
         */
        if (This._shouldRedirectReadsToInverseMap) {
            This._console.info(`Auto redirecting query to inverse map for row ${rowKey}`);
            return This._findRowsByRowKeyInverted(tableName, rowKey);
        }
        var q = new azure.TableQuery();
        q = q.where("RowKey eq ?", azTableDataSerializer.encodeKey(rowKey));
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsWithColumnValue = function (tableName, columnName, columnValue) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where(
            columnName + " eq " + azTableDataSerializer.queryType(typeof columnValue),
            azTableDataSerializer.encodeKey(columnValue)
        );
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsByRowKeyAndColumnValue = function (
        tableName,
        rowKey,
        columnName,
        columnValue
    ) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where(
            "RowKey eq ? && " + columnName + " eq " + azTableDataSerializer.queryType(typeof columnValue),
            azTableDataSerializer.encodeKey(rowKey),
            azTableDataSerializer.encodeKey(columnValue)
        );
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsByRowKeyOrColumnValue = function (
        tableName,
        rowKey,
        columnName,
        columnValue
    ) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where(
            "RowKey eq ? || " + columnName + " eq " + azTableDataSerializer.queryType(typeof columnValue),
            azTableDataSerializer.encodeKey(rowKey),
            azTableDataSerializer.encodeKey(columnValue)
        );
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsByPartitionKeyAndColumnValue = function (
        tableName,
        partitionKey,
        columnName,
        columnValue
    ) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where(
            "PartitionKey eq ? && " + columnName + " eq ?",
            azTableDataSerializer.encodeKey(partitionKey),
            azTableDataSerializer.encodeKey(columnValue)
        );
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsByPartitionKeyOrColumnValue = function (
        tableName,
        partitionKey,
        columnName,
        columnValue
    ) {
        const This = this;
        var q = new azure.TableQuery();
        q = q.where(
            "PartitionKey eq ? || " + columnName + " eq ?",
            azTableDataSerializer.encodeKey(partitionKey),
            azTableDataSerializer.encodeKey(columnValue)
        );
        return This._findRows(tableName, q);
    };

    AzTableService.prototype.findRowsByPartitionKeyLessThanEqualTo = function (tableName, value) {
        const This = this;
        let q = new azure.TableQuery();
        q = q.where("PartitionKey <= ?", azTableDataSerializer.encodeKey(value));

        return This._findRows(tableName, q);
    };

    exports.Service = AzTableService;
})();