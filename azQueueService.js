"use strict";
const azure = require("azure-storage");
const promisify = require("@withjoy/promisify");

let retryOptions = {
    retryCount: 3,
    retryInterval: 10000,
    minRetryInterval: 2000,
    maxRetryInterval: 20000
};

let AzQueueService = function(telemetry, options) {
    this._telemetry = telemetry;
    let retryOperations = new azure.ExponentialRetryPolicyFilter(retryOptions);
    this._service = options.nativeService || new azure.createQueueService(
        options.azureStorageAccount,
        options.azureStorageAccessKey
    ).withFilter(retryOperations);
};

AzQueueService.prototype.createQueueIfNotExists = promisify(function(queueName, callback) {
    return this._service.createQueueIfNotExists(queueName, function(err, result) {
        if(err) {
            this._telemetry.error("error generating queue");
            this._telemetry.error(err);
            return callback(err);
        }
        this._telemetry.info("created queue");
        return callback(null, result);
    });
});

AzQueueService.prototype.peakMessage = promisify(function (queueName, callback) {
    return this._service.peakMessages(queueName, function(err, messages){
        if(err) return callback(err);
        return callback(null, messages);
    });
});

AzQueueService.prototype.createMessage = promisify(function(queueName, message, callback) {
        return this._service.createMessage(queueName, message, function(err, result) {
            if(err) return callback(err);
            return callback(null, result);
        });
});

AzQueueService.prototype.createMessageWithDelay = promisify(function(queueName, message, timeToShowMessage, callback) {
    let options = {};
    options.visibilityTimeout = Math.floor((timeToShowMessage - Date.now()) / 1000);
    if(options.visibilityTimeout < 0) {
        options.visibilityTimeout = null;
    }
    return this._service.createMessage(queueName, message, options, function(err, result) {
        if(err) return callback(err);
        return callback(null, result);
    });
});

AzQueueService.prototype.getMessages = promisify(function (queueName, callback) {
    return this._service.getMessages(queueName, function(err, messages) {
        if(err) return callback(err);
        return callback(null, messages);
    });
});

AzQueueService.prototype.deleteMessage = promisify(function(queueName, messageId, messagePopReceipt, callback) {
    return this._service.deleteMessage(queueName, messageId, messagePopReceipt, function(err, success) {
        if(err) return callback(err);
        return callback(null, success);
    });
});


exports.Service = AzQueueService;