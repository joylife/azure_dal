(function () {

    "use strict";

    var oneByOne = require("@withjoy/oneByOne");

    exports.buildResultStream = function (azService, containerName, prefix) {
        var service = azService.nativeObject.bind(azService);
        var continuation = null;
        var arrayStream = oneByOne.createObjectSourceStream({highWaterMark: 0}, function (pushDataCallback) {
            return service().listBlobsSegmentedWithPrefix(containerName, prefix, continuation, function (err, result) {
                if (err) {
                    return pushDataCallback(err);
                }
                continuation = result.continuationToken;
                pushDataCallback(null, result.entries, !continuation);
            });
        });

        var arraySplitTransform = oneByOne.createArraySplitTransformStream({highWaterMark: 0});
        var mapTransform = oneByOne.createObjectMapTransformStream({highWaterMark: 0}, function (entry, callback) {
            return callback(null, entry);
        });

        return arrayStream.pipe(arraySplitTransform).pipe(mapTransform);
    };
})();
