(function () {
    "use strict";

    var assert = require("assert");
    var extend = require("extend");
    var sinon = require("sinon");

    // var config = require("@withjoy/config").getConfig();
    var AzTableService = require("./azTableService").Service;
    var AzTable = require("./azTable").Table;

    var stubs = require("./azure_dal.stubs");
    var nullConsole = {
        log: () => {},
        error: () => {},
        warn: () => {},
        info: () => {}
    };


    var createAzTableService = function (options, nativeService) {
        if (!nativeService) {
            nativeService = new stubs.InMemoryTableService();
        }

        return new AzTableService(nullConsole, extend(true, {}, options, {nativeService: nativeService}))
    };

    var mkProps = function (x, y, extraInfo) {
        return extend(true, {}, extraInfo, {PartitionKey: x, RowKey: y});
    };


    var describeTests = function () {
        describe("azContainerService", function () {

        });

        describe("azTableService", function () {
            describe("insertEntity", function () {
                it("insertEntity calls insertEntity", function (callback) {
                    var tableService = createAzTableService();
                    tableService.insertEntity("testTable", mkProps("x", "y", {stuff: "stuff"}), function (err) {
                        assert.ifError(err);
                        assert.equal(tableService._service.insertEntity._original.callCount, 1);
                        assert.equal(tableService._service.insertOrReplaceEntity._original.callCount, 0);
                        return callback();
                    });
                });

                it("insertEntity calls insertEntity and insertOrReplaceEntity", function (callback) {
                    var tableService = createAzTableService({shouldMaintainInverseMap: true});
                    tableService.insertEntity("testTable", mkProps("x", "y", {stuff: "stuff"}), function (err) {
                        assert.ifError(err);
                        assert.ifError(err);
                        assert.equal(tableService._service.insertEntity._original.callCount, 1);
                        assert.equal(tableService._service.insertOrReplaceEntity._original.callCount, 1);
                        return callback();
                    });
                });

                it("insertEntity immediately terminates if error is non recoverable", function (callback) {
                    this.timeout(30000);
                    this.slow(30000 * 2);
                    var nativeService = new stubs.InMemoryTableService();
                    nativeService.insertEntity = sinon.spy(function () {
                        var callback = arguments[arguments.length - 1];
                        return setTimeout(function () {
                            var err = new Error("Fake error");
                            err.isFake = true;
                            return callback(err);
                        }, 20);
                    });
                    var tableService = createAzTableService(null, nativeService);

                    tableService.insertEntity("testTable", mkProps("x", "y", {stuff: "stuff"}), function (err) {
                        assert(err);
                        assert.equal(err.isFake, true);
                        assert(tableService._service.insertEntity._original.callCount, 1, "Retrying for non recoverable errors?");
                        return callback();
                    });
                });

                it("insertEntity terminates with error if azure is repeatedly failing", function (callback) {
                    this.timeout(30000);
                    this.slow(30000 * 2);
                    var nativeService = new stubs.InMemoryTableService();
                    nativeService.insertEntity = sinon.spy(function () {
                        var callback = arguments[arguments.length - 1];
                        return setTimeout(function () {
                            var err = new Error("something ECONNRESET");
                            err.isFake = true;
                            return callback(err);
                        }, 20);
                    });
                    var tableService = createAzTableService(null, nativeService);

                    tableService.insertEntity("testTable", mkProps("x", "y", {stuff: "stuff"}), function (err) {
                        assert(err);
                        assert.equal(err.isFake, true);
                        assert(tableService._service.insertEntity._original.callCount >= 3);
                        return callback();
                    });
                });

                it("insertEntity succeeds when azure is temporarily down", function (callback) {
                    this.timeout(30000);
                    this.slow(30000 * 2);
                    var nativeService = new stubs.InMemoryTableService();
                    var oldInsertEntity = nativeService.insertEntity;
                    var callCount = 0;
                    nativeService.insertEntity = sinon.spy(function () {
                        callCount++;

                        //fail first two times, then forward to the real call
                        if (callCount <= 2) {
                            var callback = arguments[arguments.length - 1];
                            return setTimeout(function () {
                                var err = new Error("blahblah ETIMEDOUT");
                                err.isFake = true;
                                return callback(err);
                            }, 20);
                        }

                        oldInsertEntity.apply(this, arguments);
                    });
                    var tableService = createAzTableService(null, nativeService);

                    tableService.insertEntity("testTable", mkProps("x", "y", {stuff: "stuff"}), function (err) {
                        assert.ifError(err);
                        assert(tableService._service.insertEntity._original.callCount >= 3);
                        assert.equal(oldInsertEntity.callCount, 1);
                        return callback();
                    });
                });

            });
        });
    };
    describeTests();
})();
