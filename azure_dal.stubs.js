"use strict";

var sinon = require("sinon");

class InMemoryTableService {
    constructor () {
        const This = this;
        This._state = {};
        sinon.spy(This, "insertEntity");
        sinon.spy(This, "insertOrReplaceEntity");
        sinon.spy(This, "deleteEntity");
        sinon.spy(This, "mergeEntity");
        sinon.spy(This, "retrieveEntity");
    }

    insertEntity() {
        //console.log("insertEntity", JSON.stringify(Array.prototype.slice.call(arguments)));
        return arguments[arguments.length - 1](null);
    }
    insertOrReplaceEntity() {
        //console.log("insertOrReplaceEntity", JSON.stringify(Array.prototype.slice.call(arguments)));
        return arguments[arguments.length - 1](null);
    }
    deleteEntity() {
        //console.log("deleteEntity", JSON.stringify(Array.prototype.slice.call(arguments)));
        return arguments[arguments.length - 1](null);
    }
    mergeEntity() {
        //console.log("mergeEntity", JSON.stringify(Array.prototype.slice.call(arguments)));
        return arguments[arguments.length - 1](null);
    }
    retrieveEntity() {
        //console.log("retrieveEntity", JSON.stringify(Array.prototype.slice.call(arguments)));
        return arguments[arguments.length - 1](null);
    }
}

exports.InMemoryTableService = InMemoryTableService;