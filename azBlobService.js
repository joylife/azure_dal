(function () {
    "use strict";

    var azure = require("azure-storage");
    var azContainer = require("./azContainer");
    var azBlobResultStream = require("./_azBlobResultStream");

    /**
     *
     * @param consoleObj telemetry or console
     * @param options Should contain: azureStorageAccount, azureStorageAccessKey
     * @constructor
     */

     let retryOptions = {
         retries: 3,
         minTimeout: 2000,
         maxTimeout: 10000,
         randomize: true
     };

    const AzBlobService = function (consoleObj, options) {
        var This = this;
        let retryOpp = new azure.ExponentialRetryPolicyFilter(retryOptions);
        This._console = consoleObj;
        This._service = options.nativeService || new azure.createBlobService(
            options.azureStorageAccount,
            options.azureStorageAccessKey
        ).withFilter(retryOpp);
    };

    /**
     * Returns a AzContainer object after creating a container if did not already exist.
     * @param containerName name of the container
     * @param callback (err, wasCreated, azContainerObj)
     */
    AzBlobService.prototype.createContainerIfNotExists = function (containerName, callback) {
        var This = this;
        return This._service.createContainerIfNotExists(containerName, function (err, result) {
            if (err) {
                return setImmediate(callback.bind(null, err));
            }

            var container = new azContainer.Container(This, containerName);
            return setImmediate(callback.bind(null, null, result, container));
        });
    };

    AzBlobService.prototype.nativeObject = function () {
        var This = this;
        return This._service;
    };

    /**
     * @this AzBlobService
     * @param {string} containerName Name of the container to delete
     * @param {function} callback (err)
     * @returns {*}
     */
    AzBlobService.prototype.destroyContainer = function (containerName, callback) {
        var This = this;
        return This._service.deleteContainer(containerName, callback);
    };

    /**
     * Generate a Shared Access Signature to delegate access
     * @this AzBlobService
     * @param containerName
     * @param blobId
     * @param policy
     * @returns {*|string|Object}
     */
    AzBlobService.prototype.generateSharedAccessSignature = function (containerName, blobId, policy) {
        var This = this;
        var policyObj = policy.getData();
        return This._service.generateSharedAccessSignature(containerName, blobId, policyObj);
    };

    AzBlobService.prototype.getUrl = function (containerName, blobId) {
        var This = this;
        return This._service.getUrl(containerName, blobId);
    };

    AzBlobService.prototype.getBlobProperties = function (containerName, blobId, callback) {
        var This = this;
        return This._service.getBlobProperties(containerName, blobId, callback);
    };

    AzBlobService.prototype.createBlobWritableStream = function (containerName, blobId, options) {
        var This = this;
        return This._service.createWriteStreamToBlockBlob(containerName, blobId, options);
    };

    AzBlobService.prototype.createBlobWithReadableStream = function (containerName, blobId, stream, options, callback) {
        var This = this;
        return This._service.createBlockBlobFromStream(
            containerName,
            blobId,
            stream,
            options.contentLength,
            options,
            callback
        );
    };

    AzBlobService.prototype.getHost = function () {
        var This = this;
        return This._service.host;
    };

    AzBlobService.prototype.findBlobs = function (containerName) {
        var This = this;
        return azBlobResultStream.buildResultStream(This, containerName, null);
    };

    AzBlobService.prototype.deleteBlob = function (containerName, blobId, callback) {
        var This = this;
        return This._service.deleteBlob(containerName, blobId, callback);
    };

    exports.Service = AzBlobService;

})();