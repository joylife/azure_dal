(function () {
    "use strict";
    var azure = require("azure-storage");
    var perms = azure.BlobUtilities.SharedAccessPermissions;

    var AccessPolicy = function () {
        var This = this;
        This._data = {};
    };

    AccessPolicy.prototype.setStart = function (date) {
        var This = this;
        This._data.Start = date.getTime();
    };


    AccessPolicy.prototype.setExpiry = function (date) {
        var This = this;
        This._data.Expiry = date.getTime();
    };


    AccessPolicy.prototype.setPermissions = function (arr) {
        var This = this;
        This._data.Permissions = arr.join("");
    };

    AccessPolicy.prototype.getData = function () {
        var This = this;
        return {
            AccessPolicy: This._data
        };
    };

    AccessPolicy.permissions = {
        read: perms.READ,
        write: perms.WRITE,
        list: perms.LIST
    };

    exports.AccessPolicy = AccessPolicy;
})();